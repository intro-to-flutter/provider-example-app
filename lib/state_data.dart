import 'package:flutter/material.dart';

class StateData with ChangeNotifier{
  String city = "Ankara";

   void setCity(String city) {
    this.city = city;
    notifyListeners();
  }
}